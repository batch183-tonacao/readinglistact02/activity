
/*
	Create a function that will accept the name of the user and will print each character of the name in reverse order. If the length of the name is more than seven characters, the consonants of the name will only be printed in the console.

*/
let askName = prompt("Please Enter your name: ");

if (askName.length < 7)
	{

		for (let counter = askName.length - 1; counter >= 0; counter--)
			{
				console.log(askName[counter]);
			}
	}
else
	{
		for (let counter = askName.length - 1; counter >= 0; counter--)	
		{
			if (askName[counter].toLowerCase() != 'a' && askName[counter].toLowerCase() != 'e' && 
				askName[counter].toLowerCase() != 'i' && askName[counter].toLowerCase() != 'o' && 
				askName[counter].toLowerCase() != 'u')
			{
				console.log(askName[counter]);
			}
		}
	}

/*

Create an array of students with the following value.
let students = [John, Jane, Joe];
Find the following information using array methods: mutators and accessors: 
1.) Total size of an array.
2.) Adding a new element at the beginning of an array. (Add Jack)
3.) Adding a new element at the end of an array. (Add Jill)
4.) Removing an existing element at the beginning of an array .
5.) Removing an existing element at the end of an array.
6.) Find the index number of the last element in an array.
7.) Print each element of the array in the browser console.
To enable screen reader support, press Ctrl+Alt+Z To learn about keyboard shortcuts, press Ctrl+slash
 

*/

let students = ['John', 'Jane', 'Joe'];

console.log(students);
// displays the length of an array
console.log("The total size of the array is " + students.length + ".");

console.log("Adding Jack at the beginning of the array.");
// adds an element/elements at the beginning of an array
students.unshift('Jack');
console.log(students);

console.log("Adding Jill at the end of the array.");
// adds an element/elements at the end of an array
students.push('Jill');
console.log(students);

console.log("Remove an element at the beginning of the array.");
// removes an element/elements at the beginning of an array
students.shift();
console.log(students);

console.log("Remove an element at the end of the array.");
// removes an element/elements at the end of an array
students.pop();
console.log(students);

console.log("The last index number of the array is " + (students.length-1) + ".");
console.log(students);

/*
	Using the students array, create a function that will look for the index of a given student and will remove the name of the student from the array list.

	Expected output:
	//Initial content of arrays:
	[John, Jane, Joe]

	//After finding the index and removing the student "Jane"
	[John, Joe]
	

*/


let searchName = prompt("Select a name from the list to be removed: " + students);

for (let counter = students.length-1; counter >= 0; counter--)
{

	if (searchName.toLowerCase() == students[counter].toLowerCase())
	{
		console.log("Name selected is " + searchName + " and will be removed from the array.");
		students.splice(counter,1);
		console.log(students);

	break
	}
	
}


/*
Create a "person" object that contains properties first name, last name, nickname, age, address(contains city and country), friends(with at least 3 friends with the first name), and a function property that contains console.log() displaying text about self introduction about name, age, address and who is his/her friends.

Expected output:
My Name is Juan Dela Cruz, but you can call me J. I am 25 years old and I live in Quezon City, Philippines. My friends are John, Jane, and Joe.

Note: The ouput will be display through the function property inside the object variable. You may check about this keyword to solve the output.

To enable screen reader support, press Ctrl+Alt+Z To learn about keyboard shortcuts, press Ctrl+slash
 
*/

let person = {
	firstName: "Juan",
	lastName: "Dela Cruz",
	nickName: 'J',
	age: 25,
	friends: ['John', 'Jane', 'Joe'],
	address:
		{
			city: "Quezon City",
			country: "Philippines",
		},
	
};

function display(){
	console.log("My name is " + person.firstName + " " + person.lastName + ", but you can call me " + person.nickName + ". I am " + person.age + " years old and I live in " + person.address.city + ", " + person.address.country + ". My friends are " + person.friends.join(", ") + ".");
}

display();


/*
Create another copy of the "person" object use in the previous activity and apply the following ES6 updates:
1. Apply the JavaScript Destructuring Assignment in the object variable.
2. Use JavaScript Template Literals in displaying the text inside the function property of the object.
3. Run the program and check if it still produce the expected output.
To enable screen reader support, press Ctrl+Alt+Z To learn about keyboard shortcuts, press Ctrl+slash
*/ 

  



